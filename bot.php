<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
ini_set('error_log', dirname(__FILE__) .'/error_logs.txt');
error_reporting(E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

include 'Telegram.php';
include 'bd/bd_connect.php';
include 'funcoes/funcoes.classes.php';
include 'dados_bot.php';
include 'textos_bot.php';

$Chat_ID=$tlg->ChatID ();
$User_ID=$tlg->UserID ();
$User_Nome=$tlg->FirstName ();
$Text=$tlg->Text ();
$Msg_ID=$tlg->MessageID ();
$Username=@$tlg->Username ();
$Callback_ID=@$tlg->Callback_ID ();

$bd=new BD ($c);

	if ($bd->ID_guardado ($User_ID) == false){

	$bd->guarda_ID ($User_ID);

	}

@list ($comando, $value)=explode ('_', $Text,2);

$dados_user=$bd->DadosUser ($User_ID);

if (isset ($Callback_ID)){

$tlg->answerCallbackQuery([
'callback_query_id' => $Callback_ID
]);

}
switch ($comando):

case '/start': // Comando de inicio

$option = [
[
$tlg->buildInlineKeyBoardButton('Deixar de receber noticias.','','/notificacoes_'.$dados_user ['notificacao'])
],
[
$tlg->buildInlineKeyBoardButton('Ajuda','','/help')
],
[
$tlg->buildInlineKeyBoardButton('Sobre','','/info')
]
];

if (isset ($Callback_ID)){

$tlg->editMessageText ([
'chat_id' => $Chat_ID,
'message_id' => $Msg_ID,
'text' => $texto ['start'],
'parse_mode' => 'html',
'reply_markup' => $tlg->buildInlineKeyBoard($option)
]);

}else {

$tlg->sendMessage ([
'chat_id' => $Chat_ID,
'text' => $texto ['start'],
'parse_mode' => 'html',
'reply_markup' => $tlg->buildInlineKeyBoard($option)
]);

}

break;
case '/help':

$option=[[
$tlg->buildInlineKeyBoardButton('《 Voltar','','/start')
]];

$tlg->editMessageText ([
'chat_id' => $Chat_ID,
'message_id' => $Msg_ID,
'text' => $texto ['help'],
'parse_mode' => 'html',
'reply_markup' => $tlg->buildInlineKeyBoard($option)
]);

break;
case '/info':

$option=[[
$tlg->buildInlineKeyBoardButton('《 Voltar','','/start')
]];

$tlg->editMessageText ([
'chat_id' => $Chat_ID,
'message_id' => $Msg_ID,
'text' => $texto ['info'],
'parse_mode' => 'html',
'reply_markup' => $tlg->buildInlineKeyBoard($option)
]);

break;
case '/notificacoes':

	if (isset ($Callback_ID)){

$bd->update ($User_ID, $value);

		if ($value == 1){
			
$option = [
[
$tlg->buildInlineKeyBoardButton('Não quero receber noticias.','','/notificacoes_0')
],
[
$tlg->buildInlineKeyBoardButton(' ✅ Quero receber noticias.','','/notificacoes_1')
],
[
$tlg->buildInlineKeyBoardButton('《 Voltar','','/start')
]];
			}else {
				
$option = [
[
$tlg->buildInlineKeyBoardButton(' ✅ Não quero receber noticias.','','/notificacoes_0')
],
[
$tlg->buildInlineKeyBoardButton('Quero receber noticias.','','/notificacoes_1')
],
[
$tlg->buildInlineKeyBoardButton('《 Voltar','','/start')
]];
				}
				
				
$tlg->editMessageText ([
'chat_id' => $Chat_ID,
'message_id' => $Msg_ID,
'text' => $texto ['notificacoes'],
'parse_mode' => 'html',
'reply_markup' => $tlg->buildInlineKeyBoard($option)
]);
		
		}

break;
endswitch;

?>